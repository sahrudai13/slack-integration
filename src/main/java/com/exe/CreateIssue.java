package com.exe;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.Scanner;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * 	This class describes how to create an issue in a project using jira REST API
 *
 * Date:10/04/2020
 * @author  Sahrudai Reddy
 */

public class CreateIssue {
	
	public void creatingIssue() {
		/*
		 * variables used through out this class
		 * 
		 */
		
		URLConnection urlConnection;
		OutputStream output;
		StringBuffer contentResponse = new StringBuffer();
		String line;
		BufferedReader reader;
		Scanner scan = new Scanner(System.in);
		String issueKey = "";
		
		/*
		 * User details for authentication
		 */
		String userName = "sahrudai";
        String passWord = "jira";
  
		try {
			// Taking issue details from user 
			 
			System.out.println("enter project key");
			String projectKey = scan.next();
			System.out.println("enter issueType id");
			int issueTypeID = scan.nextInt();
			System.out.println("enter reporter");
			String issueReporter = scan.next();
			System.out.println("enter assignee");
			String issueAssignee = scan.next();
			System.out.println("enter summary");
			String summary = scan.next();
			scan.close();
			
			//JSON objects to store the input from user
			JSONObject fields = new JSONObject();
			JSONObject project = new JSONObject();
			JSONObject assignee = new JSONObject();
			JSONObject reporter = new JSONObject();
			JSONObject issueType = new JSONObject();
			JSONObject field = new JSONObject();
			
			project.put("key",projectKey);
			assignee.put("name", issueAssignee);
			reporter.put("name", issueReporter);
			issueType.put("id", issueTypeID);
			
			fields.put("project",project);
			fields.put("assignee",assignee);
			fields.put("reporter",reporter);
			fields.put("issuetype",issueType);
			fields.put("summary",summary);
			
			field.put("fields", fields);
			
			//converting JSON object to String 
			String json = field.toString();
			
			//URL for creating an issue through REST API in jira
			
			URL url = new URL("http://localhost:8000/rest/api/2/issue");
			
			urlConnection = (HttpURLConnection) url.openConnection();
			HttpURLConnection connection = (HttpURLConnection) urlConnection;
			
			connection.setDoOutput(true);
			connection.setDoInput(true);
			
			//converting user details into Base64 
			String basicAuth = Base64.getEncoder().encodeToString((userName+":"+passWord).getBytes());
			
	       //	System.out.println(basicAuth);
	        
	        //Authorization header
			connection.setRequestProperty("Authorization", "Basic "+basicAuth);
			
			//The format
			connection.setRequestProperty("Content-Type", "application/json");
			
			//HTTP method used
			connection.setRequestMethod("GET");
			
			output = connection.getOutputStream();
			
			//Writing data to destination 
			output.write(json.getBytes());
			
			System.out.println(connection.getResponseCode());
            
			//if the connection is success
            if(connection.getResponseCode()<299)
			{
            	System.out.println("issue created");
				reader = new BufferedReader(new InputStreamReader(connection.getInputStream())); 
				while((line = reader.readLine())!= null)
				{
					contentResponse.append(line); //reading the input and storing in the StringBuffer object
				}
				reader.close();
				System.out.println(contentResponse.toString());
				
				String jsonObj = contentResponse.toString();
				
				JSONParser parse = new JSONParser();
				
				JSONObject jobj = (JSONObject)parse.parse(jsonObj);
				
				//System.out.println(jobj);
				
				issueKey =  (String) jobj.get("key");
				
				System.out.println(issueKey);
			}
            //if there is some error in connection or URL or anything
            else
			{
            	System.out.println("error");
				reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				while((line = reader.readLine())!= null)
				{
					contentResponse.append(line); //reading the error and storing in the StringBuffer object
				}
				reader.close();
				System.out.println(contentResponse.toString());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*AddingChannelToJira add= new AddingChannelToJira();
		add.addingRemoteLink(issueKey);
	*/
	}

}
