package com.exe;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class GettingAttachments {
	
public static void main(String[] args) {
		
		
		URLConnection urlConnection;
		StringBuffer contentResponse = new StringBuffer();
		String line;
		BufferedReader reader;
		Scanner scan = new Scanner(System.in);
		
		
		String userName = "sahrudai";
        String passWord = "jira";
       
		try {
			 
			// Taking issue details from user 
			System.out.println("enter the issue-key to view");
        	String issueKey = scan.next();
        	scan.close();
        	
        	
        	String issueKeyWithURL = "http://localhost:8090/rest/api/2/issue/"+issueKey+"?fields=attachment";
			URL url = new URL(issueKeyWithURL);
			
			urlConnection = (HttpURLConnection) url.openConnection();
			HttpURLConnection connection = (HttpURLConnection) urlConnection;
			
			connection.setDoOutput(true);
			connection.setDoInput(true);
			 
			String basicAuth = Base64.getEncoder().encodeToString((userName+":"+passWord).getBytes(StandardCharsets.UTF_8));
			
			connection.setRequestProperty("Authorization", "Basic "+basicAuth);
			
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			
			connection.setRequestMethod("GET");
			
			System.out.println(connection.getResponseCode());
            
            if(connection.getResponseCode()<299)
			{
				reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				while((line = reader.readLine())!= null)
				{
					contentResponse.append(line); //reading the input and storing in the StringBuffer object
				}
				reader.close();
				
				
				String jsonObj = contentResponse.toString();
				
				JSONParser parse = new JSONParser();
				
				JSONObject jobj = (JSONObject)parse.parse(jsonObj);
				
				//System.out.println(jobj);
				
				JSONObject j2 = (JSONObject) jobj.get("fields");
				
				JSONArray jsonar=(JSONArray) j2.get("attachment");
				
				//System.out.println(j2);
				
				String URI = "";
				
				for (int i = 0; i < jsonar.size(); i++) {
					
					JSONObject obja = (JSONObject) jsonar.get(i);
					URI = (String) obja.get("content");
				}
				
				System.out.println(URI);
				
				/*DownloadingAttch send =  new DownloadingAttch();
				send.downloadAttachmentsUsingContentLink(URI);*/
			}
            else
			{
            	System.out.println("else");
				reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				while((line = reader.readLine())!= null)
				{
					contentResponse.append(line); //reading the error and storing in the StringBuffer object
				}
				reader.close();
				System.out.println(contentResponse.toString());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}
