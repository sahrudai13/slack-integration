package com.exe;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.utils.URIBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.web.util.UriComponentsBuilder;

import com.jiras.AddingCommentOnIssue;


public class GettingConversationHistory {
	
	public static void main(String[] args) {
		URLConnection urlConnection;
		StringBuffer contentResponse = new StringBuffer();
		String line;
		BufferedReader reader;
		
		String botToken = "xoxp-1061741899685-1063331904899-1290267661536-c925962eac1fcc3260df921b256cc719";
		
		try {
			
			String urls = "https://slack.com/api/conversations.history?channel=" + URLEncoder.encode("C01218J3W7N","UTF-8");
			
			URL url = new URL(urls);
			
			urlConnection = (HttpURLConnection) url.openConnection();
			HttpURLConnection connection = (HttpURLConnection) urlConnection;
			
			
			connection.setDoOutput(true);
			connection.setDoInput(true);
			
			connection.setRequestProperty("Authorization","Bearer "+botToken);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
			connection.setRequestMethod("POST");
			
			if(connection.getResponseCode()<299)
			{
				reader = new BufferedReader(new InputStreamReader(connection.getInputStream())); 
				while((line = reader.readLine())!= null)
				{
					contentResponse.append(line); //reading the input and storing in the StringBuffer object
				}
				reader.close();
				
				String jsonObj = contentResponse.toString();
				System.out.println(contentResponse);
				
				JSONParser parse = new JSONParser();
				
				JSONObject jobj = (JSONObject)parse.parse(jsonObj);
				
				JSONArray jsonar = (JSONArray) jobj.get("messages");
			
				 String url1 = "";
				 String urlpr ="";
				for (int i = 0; i < jsonar.size(); i++) {
					
					JSONObject obj = (JSONObject) jsonar.get(i);
				String text =  (String) obj.get("text");
				
				System.out.println(text);
				/*if(text.equals(""))
				{
					url1=(String) obj.get("url_private");
					urlpr=(String) obj.get("url_private_download");
				}*/
					
				Pattern issuePattern = Pattern.compile("^([A-Z]+)-(\\d+)");
		    	String[] array = text.split(" ");
		    	boolean check = false;
		    	String issueKey = "";
		    	
		    	for (int j = 0; j < array.length; j++) {
		    		
					Matcher match = issuePattern.matcher(array[j]);
					if(match.find())
					{
						issueKey = array[j];
						check = true;
					}
				}
		    	
		    	if(check) {
		    		
		    		AddingCommentOnIssue add = new AddingCommentOnIssue();
		    		add.commenting(issueKey, text);
		    		
		    		System.out.println("Added Successfully");
		    		
		    	}
		    	else 
		    	{
		    		System.out.println("No issue key mentioned ");
		    	}
				}
				/*System.out.println(url1);
				System.out.println(urlpr);*/
				
			}
			else
			{
				reader = new BufferedReader(new InputStreamReader(connection.getInputStream())); 
				while((line = reader.readLine())!= null)
				{
					contentResponse.append(line); //reading the input and storing in the StringBuffer object
				}
				reader.close();
				System.out.println(contentResponse.toString());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
