package com.dowloading;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Scanner;

import javax.imageio.ImageIO;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.exe.UploadAttachmentsToSlack;

public class DownloadAttachmentsFromJira {
	
	public void downloadAttch() {
	
	URLConnection urlConnection;
	StringBuffer contentResponse = new StringBuffer();
	String line;
	BufferedReader reader;
	Scanner scan = new Scanner(System.in);
	String fileName = "";
	
	
	String userName = "sahrudai";
    String passWord = "jira";
   
	try {
		 
		// Taking issue details from user 
		System.out.println("enter the issue-key to view");
    	String issueKey = scan.next();
    	scan.close();
    	
    	
    	String issueKeyWithURL = "http://localhost:8000/rest/api/2/issue/"+issueKey+"?fields=attachment";
		URL url = new URL(issueKeyWithURL);
		
		urlConnection = (HttpURLConnection) url.openConnection();
		HttpURLConnection connection = (HttpURLConnection) urlConnection;
		
		connection.setDoOutput(true);
		connection.setDoInput(true);
		 
		String basicAuth = Base64.getEncoder().encodeToString((userName+":"+passWord).getBytes(StandardCharsets.UTF_8));
		
		connection.setRequestProperty("Authorization", "Basic "+basicAuth);
		
		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		
		connection.setRequestMethod("GET");
		
		System.out.println(connection.getResponseCode());
        
        if(connection.getResponseCode()<299)
		{
			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while((line = reader.readLine())!= null)
			{
				contentResponse.append(line); //reading the input and storing in the StringBuffer object
			}
			reader.close();
			
			
			String jsonObj = contentResponse.toString();
			
			JSONParser parse = new JSONParser();
			
			JSONObject jobj = (JSONObject)parse.parse(jsonObj);
			
			//System.out.println(jobj);
			
			JSONObject j2 = (JSONObject) jobj.get("fields");
			
			JSONArray jsonar=(JSONArray) j2.get("attachment");
			
			//System.out.println(j2);
			
			String URI = "";
			
			for (int i = 0; i < jsonar.size(); i++) {
				
				JSONObject obja = (JSONObject) jsonar.get(i);
				URI = (String) obja.get("content");
			}
			
			System.out.println(URI);
			
			
			URL imageURL = new URL(URI);
			
			String spil[]= URI.split("/");
			String store = "";
			
			for (int i = 0; i < spil.length; i++) {
				
				store = spil[i];
				String whereToSave = "C:\\Users\\Dell\\Documents\\slck\\";
				fileName = whereToSave+store;
				
				
			}
			try (BufferedInputStream in = new BufferedInputStream(new URL(URI).openStream());
					  FileOutputStream fileOutputStream = new FileOutputStream(fileName)) {
					    byte dataBuffer[] = new byte[1024];
					    int bytesRead;
					    while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
					        fileOutputStream.write(dataBuffer, 0, bytesRead);
					    }
					    System.out.println("file downloaded");
					} catch (Exception e) {
					    // handle exception
					}
			
		
			/*InputStream inputStream = null;
			try {
				inputStream = new URL(URI).openStream();
				Files.copy(inputStream, Paths.get(fileName));
				System.out.println("downloaded");
			} catch (IOException e) {
				System.out.println("Exception Occurred " + e);
			} finally {
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {
						// Ignore
					}
				}
			}*/
			/*
			UploadAttachmentsToSlack upload = new UploadAttachmentsToSlack();
			upload.uploadToSlack(URI);*/
			
			/*BufferedImage image =ImageIO.read(imageURL);
			
			ImageIO.write(image, "jpeg",new File("C:\\Users\\Dell\\Documents\\slck\\susa.jpeg"));*/
					
		}
        else
		{
        	System.out.println("else");
			reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
			while((line = reader.readLine())!= null)
			{
				contentResponse.append(line); //reading the error and storing in the StringBuffer object
			}
			reader.close();
			System.out.println(contentResponse.toString());
		}
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
UploadAttachmentsToSlack up = new UploadAttachmentsToSlack();
up.uploadToSlack(fileName);
	}

}
