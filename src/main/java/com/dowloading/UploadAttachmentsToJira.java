package com.dowloading;

import java.io.File;
import java.util.Base64;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class UploadAttachmentsToJira {
	
//public static void main(String[] args) { 
	
	public void uploadAttachments(String filePath) {
		
		Scanner scan = new Scanner(System.in);
		
		/*
		 * User details for authentication
		 */
		String userName = "sahrudai";
        String passWord = "jira";
        
        //converting user details into Base64 
        String basicAuth = Base64.getEncoder().encodeToString((userName+":"+passWord).getBytes());

        //Taking the issue-key from user 
        System.out.println("enter the issue-key to add attachment");
    	String issueKey = scan.next();
    	
    	/*System.out.println("mention the path of the file you want to upload");
	    String filePath = scan.next();*/
	
        
	    HttpClient httpclient = new DefaultHttpClient();
	    
	    //URl for adding attachment
	    HttpPost httppost = new HttpPost("http://localhost:8000/rest/api/2/issue/"+issueKey+"/attachments");
	    
	    //As per Atlassian documentation,For XSRF(Cross Site Request Forgery ) protection, using "X-Atlassian-Token" 
	    httppost.setHeader("X-Atlassian-Token", "nocheck");
	    
	    //Authorization header
	    httppost.setHeader("Authorization", "Basic "+basicAuth);
	    
	    //As uploading attachment is a multipart POST ,Using MultipartEntity from Apache HTTP components library
	    MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

	    //Taking full File-path from user to upload.[ex-(C:\Users\Dell\Documents\ex.txt)]
	    
	    
	    //The name of the multipart/form-data parameter that contains attachments must be "file"
	    entity.addPart("file", new FileBody(new File(filePath), "application/octet-stream"));
	    
	    /*  File fileToUpload = new File(filePath);
	    FileBody fileBody = new FileBody(fileToUpload, "application/octet-stream");
	    entity.addPart("file", fileBody);
	    */
	    
	    httppost.setEntity(entity);
	    try {
	    	
	    	//Executing the POST request
	    	HttpResponse response = httpclient.execute(httppost);

	 	    if(response.getStatusLine().getStatusCode() == 200)
	 	       System.out.println("attachment successfully uploaded");
	 	    else
	 	       System.out.println("attachment upload failed");
	 	    
	    } catch (Exception e) {
	       // return false;
	    }
	  
}

}
