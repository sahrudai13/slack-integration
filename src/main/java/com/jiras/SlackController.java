package com.jiras;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;

@RestController
public class SlackController {
    @RequestMapping(value = "/slash",method = RequestMethod.POST,
          consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String onReceiveSlashCommand(@RequestParam("token") String token,
							            @RequestParam("team_id") String teamId,
							            @RequestParam("team_domain") String teamDomain,
							            @RequestParam("channel_id") String channelId,
							            @RequestParam("channel_name") String channelName,
							            @RequestParam("user_id") String userId,
							            @RequestParam("user_name") String userName,
							            @RequestParam("command") String command,
							            @RequestParam("text") String text,
							            @RequestParam("response_url") String responseUrl)
    {
        
    	System.out.println(text);
    	Pattern issuePattern = Pattern.compile("^([A-Z]+)-(\\d+)");
    	String[] array = text.split(" ");
    	boolean check = false;
    	String issueKey = "";
    	
    	for (int i = 0; i < array.length; i++) {
    		
			Matcher match = issuePattern.matcher(array[i]);
			if(match.find())
			{
				issueKey = array[i];
				check = true;
			}
		}
    	
    	if(check) {
    		
    		AddingCommentOnIssue add = new AddingCommentOnIssue();
    		add.commenting(issueKey, text);
    		
    		return "Added Successfully";
    		
    	}
    	else 
    	{
    		return "No issue key mentioned ";
    	}
    	
    	
    	
    }
    
    @RequestMapping(value ="/hook",method = RequestMethod.POST,produces = "application/json", consumes = "application/json")
    @ResponseBody
    public void webhooks(@RequestBody String jsonBody)
    {
    	JSONParser parse = new JSONParser();
    	try {
			JSONObject jobj = (JSONObject)parse.parse(jsonBody);
			System.out.println(jobj);
			String event =  (String) jobj.get("webhookEvent");
			
			System.out.println(event);
			
			SlackIncomingWebhooks post = new SlackIncomingWebhooks();
			post.postingMessagesInSlack(event);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    @RequestMapping(value="/event",method = RequestMethod.POST)
    public void eventsCheck(@RequestBody String jsonBody) throws IOException, SlackApiException
    {
    	System.out.println(jsonBody);
    	JSONParser parse = new JSONParser();
    	String txt = "";
    	//String challenge = "";
		try {
			JSONObject jobj = (JSONObject)parse.parse(jsonBody);
			//System.out.println(jobj);
			//challenge = (String) jobj.get("challenge");
			JSONObject event =  (JSONObject) jobj.get("event");
			txt = (String) event.get("text");
	
			System.out.println(txt); 
			//System.out.println(challenge);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String token = "xoxb-1061741899685-1266597053587-7mT5ZLU6O1INjxAB1yuuUDRh";
		Slack slack = Slack.getInstance();
		MethodsClient methods = slack.methods(token);
		ChatPostMessageRequest request = ChatPostMessageRequest.builder()
				  .channel("#random") // 
				  .text(":smile: Hi from a bot written in Java!")
				  .build();

				// Get a response as a Java object
				ChatPostMessageResponse response = methods.chatPostMessage(request);
				//System.out.println(response);
		/*return "HTTP 200 OK" + 
				"Content-type: text/plain" + 
				challenge;*/
		
    }
   
}
