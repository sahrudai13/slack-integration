package com.jiras;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Scanner;

import org.json.simple.JSONObject;

public class AddingCommentOnIssue {
	
public  void commenting(String issueKey,String comments) {
		
		/*
		 * variables used through out this class
		 * 
		 */
		URLConnection urlConnection;
		StringBuffer contentResponse = new StringBuffer();
		String line;
		OutputStream output;
		BufferedReader reader;
		Scanner scan = new Scanner(System.in);
		
		/*
		 * User details for authentication
		 */
		String userName = "sahrudai";
        String passWord = "jira";
        
		try {
        
        	String issueKeyWithURL = "http://localhost:8000/rest/api/2/issue/"+issueKey+"/comment";
			URL url = new URL(issueKeyWithURL);
			
        	
			JSONObject comment = new JSONObject();
			comment.put("body", comments);
			
			String json = comment.toString();
			
			urlConnection = (HttpURLConnection) url.openConnection();
			HttpURLConnection connection = (HttpURLConnection) urlConnection;
			
			connection.setDoOutput(true);
			connection.setDoInput(true);
		 
			String basicAuth = Base64.getEncoder().encodeToString((userName+":"+passWord).getBytes(StandardCharsets.UTF_8));
	     
			connection.setRequestProperty("Authorization", "Basic "+basicAuth);
			
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			
			connection.setRequestMethod("POST");
			
			output = connection.getOutputStream();
			
			output.write(json.getBytes());
			
			System.out.println(connection.getResponseCode());
            
            if(connection.getResponseCode()<299)
			{
            	System.out.println("commented on "+ issueKey);
				reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				while((line = reader.readLine())!= null)
				{
					contentResponse.append(line); //reading the input and storing in the StringBuffer object
				}
				reader.close();
				System.out.println(contentResponse.toString());
			}
            else
			{
            	System.out.println("else");
				reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				while((line = reader.readLine())!= null)
				{
					contentResponse.append(line); //reading the error and storing in the StringBuffer object
				} 
				reader.close();
				System.out.println(contentResponse.toString());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
